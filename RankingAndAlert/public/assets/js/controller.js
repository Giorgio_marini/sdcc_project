app.controller("controller", function($scope, $http, ResourceService) {

    $scope.ranking=[];
    $scope.dailyAlert=[];
    $scope.lastestAlert=[];
    $scope.days=[1,2,3,4,5];

   $scope.deleteAlert = function () {
       $scope.lastestAlert = [];
   }

   $scope.deleteRanking = function () {
       $scope.ranking = [];
   }
   $scope.deleteAlertDay = function () {
       $scope.dailyAlert= [];
   }

    $scope.getLastestAlert = function (day) {
       console.log(day);
       if(day === undefined){
           day = 1;
       }
        ResourceService.getLastestAlert({
            d : day
        }).then(function (res) {
            console.log(res);
            $scope.lastestAlert = JSON.parse(JSON.stringify(res.data)).Items;
        }, function (res){
            console.log("error");
            console.log(res);
        });
    };

    $scope.getRankingByDate = function () {
        ResourceService.getRankingByDate().then(function (res) {
            console.log(res);
            $scope.ranking = JSON.parse(JSON.stringify(res.data)).Items;
        },function(res){
                console.log("err");
                console.log(res);
        });
        console.log("query for data")
    };

    $scope.getAlertByDate = function() {
        ResourceService.getAlertByDate().then(function (res) {
            console.log(res);
            $scope.dailyAlert = JSON.parse(JSON.stringify(res.data)).Items;
        }, function (res) {
                console.log("err");
                console.log(res)

        });
        console.log("query for Alert");
    }
});
