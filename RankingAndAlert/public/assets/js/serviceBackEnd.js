app.service("ResourceService", function ($q,$http) {

    var ajax = function (method, url, data) {

        var deferred = $q.defer();
        var request = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        if (method === 'GET' || method === 'DELETE') {
            request.params = data;
        } else {
            request.data = data;
        }
        $http(request).then(function (response) {
            deferred.resolve(response);
        }, function (response, status) {
            deferred.reject({data: response, status: status});
        });
        return deferred.promise;
    };

    this.getAlertByDate=function (params) {
        return ajax("GET","http://localhost:8080/getAlertForDate",params)
    }

    this.getRankingByDate=function (params) {
        return ajax("GET","http://localhost:8080/getRankingForDate",params)
    }

    this.getLastestAlert=function (params) {
        return ajax("GET","http://localhost:8080/getLastestAlert", params)
    }
});