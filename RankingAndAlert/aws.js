var express = require('express');

var AWS = require('aws-sdk');
AWS.config.update({
    region: "eu-west-1",
    endpoint: "dynamodb.eu-west-1.amazonaws.com"
});
var db = new AWS.DynamoDB();


module.exports = AWS;
