var http = require('http');
var url = require('url');
var express = require('express');
var path = require('path');
var aws = require('./aws.js');

var app = express();
app.use(express.static('documentation'));
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));


/**
 * Return the Ranking for the current day
 *
 */
app.get('/getRankingForDate', function(req,res){
    var date = new Date();
    var docClient = new aws.DynamoDB.DocumentClient();
    var dateQuery = date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear();
    var params = {
        ExpressionAttributeNames: {"#Date": "Date"},
        ExpressionAttributeValues: {":Date": dateQuery},
        FilterExpression: "#Date = :Date",
        Limit: 10,
        TableName: "ranking"
    };
    docClient.scan(params,function (err,data) {
        if(err){
            console.error("Unable to Scan item. Error JSON:", JSON.stringify(err, null, 2));
        }else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.send(data);
        }
    })
})

/**
 * Return the alerts for the current day
 */
app.get('/getAlertForDate',function (req,res) {
    var date = new Date();
    var docClient = new aws.DynamoDB.DocumentClient();
    var dateQuery = date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear();
    console.log(dateQuery);
    var params = {
        ExpressionAttributeNames: {"#Date": "Date"},
        ExpressionAttributeValues: {":Date": dateQuery},
        FilterExpression: "#Date = :Date",
        Limit: 20,
        TableName: "alert"
    };
    docClient.scan(params,function (err,data) {
        if(err){
            console.error("Unable to Scan item. Error JSON:", JSON.stringify(err, null, 2));
        }else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.send(data);
        }
    })
})
/**
 * Return the lastest alert in function of a period
 *
 */
app.get('/getLastestAlert', function (req,res) {

    var interval = req.query.d;
    var date = new Date();
    var docClient = new aws.DynamoDB.DocumentClient();
    var highExt = date.getTime();
    console.log(highExt);
    var lowExt = date.getTime() - (interval*1000*60*60*24);
    console.log(lowExt);

    var params = {
        TableName: "alert",
        FilterExpression: "#Timestamp BETWEEN :start_ts AND :stop_ts",
        ExpressionAttributeNames: {
            "#Timestamp":"Timestamp",
        },
        ExpressionAttributeValues: {
            ":start_ts": lowExt,
            ":stop_ts": highExt
        }
    };
    docClient.scan(params,function (err,data) {
        if(err){
            console.error("Unable to Scan item. Error JSON:", JSON.stringify(err, null, 2));
        }else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.send(data);
            /* res.send("on my page");*/
        }
    })


})
app.listen(8080,function(){
  console.log("Listen on port 8080");
})

