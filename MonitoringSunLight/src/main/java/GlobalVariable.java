public class GlobalVariable {
    public static final String bucket = "configfilesmart";
    public static final String key = "configAWS.json";

    public static String TOPICSunLight = "SunLight";
    public static String TOPIControl  = "ControlLamp";
    public static double DISTANCECONTROL = 0.50;

    //range to describe the traffic on a street
    public static double rangeMin = 0.0;
    public static double rangeMax = 100.0;
    public static double scaleFactorTraffic =1/3;

    //range the class of Traffic
    public static  int trafficMin =30;
    public static  int trafficMax =70;


    //percentage for the traffic reduction
    public static  double firstClassTraffic = 0.30;
    public static  double secondClassTraffic = 0.70;



}
