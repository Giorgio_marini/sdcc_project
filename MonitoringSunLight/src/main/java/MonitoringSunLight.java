import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import redis.clients.jedis.Jedis;


import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.Random;

public class MonitoringSunLight {
    private static String ipKafkaBroker;
    private static Jedis jedis;

    public static void main(String[] args) throws Exception {
        readArgs(args);
        settingGlobalVariable();
        runConsumer();
    }

    /**
     * setting of Global variable with the Bucket of S3
     */
    private static void settingGlobalVariable() {

        AmazonS3 s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
        try {
            System.out.println("Downloading an object");
            s3Client.getObject(
                    new GetObjectRequest(GlobalVariable.bucket, GlobalVariable.key),
                    new File("config.json")
            );

            JSONParser jsonParser = new JSONParser();
            try {
                Object obj = jsonParser.parse(new FileReader("config.json"));
                JSONObject jsonObject = (JSONObject) obj;
                JSONObject object = (JSONObject) jsonObject.get("MonitoringSunLight");
                GlobalVariable.TOPICSunLight = object.get("TOPICSunLight").toString();
                GlobalVariable.TOPIControl = object.get("TOPIControl").toString();
                GlobalVariable.DISTANCECONTROL = Double.parseDouble(object.get("DISTANCECONTROL").toString());
                GlobalVariable.rangeMin = Double.parseDouble(object.get("rangeMin").toString());
                GlobalVariable.rangeMin = Double.parseDouble(object.get("rangeMax").toString());
                GlobalVariable.scaleFactorTraffic = Double.parseDouble(object.get("scaleFactorTraffic").toString());
                GlobalVariable.trafficMin = Integer.parseInt(object.get("trafficMin").toString());
                GlobalVariable.trafficMax = Integer.parseInt(object.get("trafficMax").toString());
                GlobalVariable.firstClassTraffic = Double.parseDouble(object.get("firstClassTraffic").toString());
                GlobalVariable.secondClassTraffic = Double.parseDouble(object.get("secondClassTraffic").toString());
            }catch (Exception e){
                e.printStackTrace();
            }

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    /**
     * RunConsumer handle MAPE-K system, with DSP and execute a plan
     * @throws Exception
     */
    static void runConsumer() throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1000);
        /**
         * Zookeeper and Server kafka Configuration
         */
        Properties p = initProperties();
        jedis = new Jedis("localhost",6379);
        jedis.connect();
        jedis.flushAll();
        jedis.flushDB();
        System.out.println("stamp: "+ jedis.ping());

        FlinkKafkaConsumer09<String> sunStreaming = new FlinkKafkaConsumer09<>(
                GlobalVariable.TOPICSunLight, new SimpleStringSchema(), p);

        FlinkKafkaProducer09<String> controlFlow = new FlinkKafkaProducer09<String>(ipKafkaBroker+":9092",
                GlobalVariable.TOPIControl, new SimpleStringSchema());
        controlFlow.setFlushOnCheckpoint(true);
        controlFlow.setLogFailuresOnly(false);


        sunStreaming.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<String>(Time.seconds(200)) {
            @Override
            public long extractTimestamp(String s) {
                String[] tmp = s.split(",");
                return Long.parseLong(tmp[0]);
            }
        });

        DataStream<Tuple3<Long,Integer,Double>> serializeSunStreaming =
                env.addSource(sunStreaming)
                        .flatMap(new serializeStream());

        DataStream<Tuple3<Long,Integer,Double>> avgToRefactorLight = serializeSunStreaming
                .keyBy(1)
                .timeWindow(Time.minutes(2))
                .apply(new AggregateValueSunLight());

        DataStream<Tuple3<Long,Integer, Double>> doCorrection = avgToRefactorLight
                .keyBy(1)
                .flatMap(new manageLamp())
                .setParallelism(1);

        doCorrection.writeAsText("controlTuple.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        DataStream<String> doCorrectionString = doCorrection.flatMap(new StringfyTuple());
        doCorrectionString.addSink(controlFlow);
        env.execute();
    }

    private static Properties initProperties() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ipKafkaBroker+":9092");
        properties.setProperty("zookeeper.connect", ipKafkaBroker+":2181");
        properties.setProperty("group.id", "SunLightConsumer");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return properties;
    }



    private static void readArgs(String[] args) {
        if(args.length!=1){
            ipKafkaBroker="34.253.140.181";
        }else {
            ipKafkaBroker=args[0];
        }
    }

    /**
     * Serialize the Stream with the variabile
     */
    private static class serializeStream implements FlatMapFunction<String,Tuple3<Long,Integer,Double>> {

        @Override
        public void flatMap(String s, Collector<Tuple3<Long, Integer, Double>> collector) throws Exception {
            String[] tmp = s.split(",");
            long timestamp = Long.parseLong(tmp[0]);
            int index = Integer.parseInt(tmp[1]);
            double intensity = Double.parseDouble(tmp[2]);
            collector.collect(new Tuple3<Long, Integer, Double>(timestamp,index,intensity));
        }
    }

    /**
     * Aggregate the value of the sun intensity into the window
     */
    private static class AggregateValueSunLight implements WindowFunction<Tuple3<Long,Integer,Double>,Tuple3<Long,Integer,Double>,Tuple,TimeWindow> {
        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple3<Long, Integer, Double>> iterable, Collector<Tuple3<Long, Integer, Double>> collector) throws Exception {
            String key = "";
            double avgSunLight = 0,sum = 0, count = 0, intensityLight;
            for (Tuple3<Long, Integer, Double> t :iterable) {
                key = Integer.toString(t.f1);
                sum += t.f2;
                count ++;
            }
            double traffic = getTrafficCar();
            avgSunLight = sum/count;
            intensityLight = getIntensity(avgSunLight, traffic);
            collector.collect(new Tuple3<Long, Integer, Double>(timeWindow.getEnd(),Integer.parseInt(key),intensityLight));
        }

        /**
         * this is a fake method REST to take the value of the car of The Street on a scale of [0,100] like car to minuts
         * @return
         * the value of the Traffic in function of the time
         */
        private double getTrafficCar() {

            Random r = new Random();
            double randomValue = GlobalVariable.rangeMin + (GlobalVariable.rangeMax - GlobalVariable.rangeMin) * r.nextDouble();
            return randomValue;
        }

        private double getIntensity(double avgSunLight, double traffic) {
            if(avgSunLight<=7) {
                if (traffic >= GlobalVariable.trafficMax) {
                    return 10 - avgSunLight;
                } else if (traffic < GlobalVariable.trafficMax && traffic >= GlobalVariable.trafficMin) {
                    return (10 - avgSunLight)*GlobalVariable.secondClassTraffic;
                } else if (traffic < GlobalVariable.trafficMin) {
                    return (10 -avgSunLight)*GlobalVariable.firstClassTraffic;
                }
            }else return 0.0;
            return 0.0;
        }

    }

    /**
     * Control the status of new value with cached value and emit new status
     */
    private static class manageLamp implements FlatMapFunction<Tuple3<Long, Integer, Double>, Tuple3<Long,Integer,Double>> {

        private double checkDistance(double avgSunLight, double cacheValue) {
            return Math.abs(avgSunLight-cacheValue);
        }

        @Override
        public void flatMap(Tuple3<Long, Integer, Double> in, Collector<Tuple3<Long, Integer, Double>> collector) throws Exception {

            String key = Integer.toString(in.f1);
            double intensityLight = in.f2;

            if(jedis.get(key)==null || jedis.get(key).equals("(nil)") || jedis.get(key).equals("")){
                String value = Double.toString(in.f2);
                jedis.set(key,value);
                collector.collect(new Tuple3<Long, Integer, Double>(in.f0,Integer.parseInt(key),intensityLight));

            }else{
                double cacheValue = Double.parseDouble(jedis.get(key));
                double d = checkDistance(intensityLight,cacheValue);
                if(d>GlobalVariable.DISTANCECONTROL){
                    jedis.del(key);
                    jedis.set(key,Double.toString(intensityLight));
                    collector.collect(new Tuple3<Long, Integer, Double>(in.f0,Integer.parseInt(key),intensityLight));
                }
            }
        }
    }

    /**
     * manage output for controller Topic
     */
    private static class StringfyTuple implements FlatMapFunction<Tuple3<Long,Integer,Double>,String> {
        @Override
        public void flatMap(Tuple3<Long, Integer, Double> in, Collector<String> collector) throws Exception {
            String ret = in.f0+","+in.f1+","+in.f2;
            collector.collect(ret);
        }
    }
}
