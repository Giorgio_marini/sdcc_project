# README #

Questa repository contiene il codice di un sistema di monitoraggio e controllo per una Smart cities.
é composto da diversi moduli:

* LampStreaming
* MonitoringSunLight
* MonitoringLight
* SinkNode
* RankingAndAlert

Il primo modulo consiste nella generazione dei dati creati dai lampioni, il secondo forma un sistema MAPE-K per il monitoring e l'autoadeguamento
dell'intensità luminosa, MonitoringLight consiste in un sista di monitoring dell'illuminazione della città attraverso una applicazione DSP.
Il SinkNode, si occupa della memorizzazione degli output del nodo di monitoring, mentre il RankingAndAlert è costituito da un server Web di una 
web application che si occupa di mostrare i risultati ottenuti.

Per far funzionare l'intero sistema abbiamo bisogno anche dell'utilizzo di due Broker Kafka che permettono l'ingestion dei dati tra i vari componenti,
ma nel repository è stata omessa la loro AMI, ma sono state fornite delle slide per la configurazione di un Broker Kafka su un'istanza EC2.

### Getting Start ###

* Clone il repository
* scegliere il servizio da eseguire
* Build il jar di esso attraverso *mvn package* nella directory del nodo o tramite Ide quale Intellj
* far eseguire il jar fornendo come parametro l'indirizzo IP del broker kafka


### Contribution guidelines ###

è possibile consultare altre guide sull'istallazione di kakfa e il deploy su ec2 e altri framework di utilizzo su:

* https://kafka.apache.org/
* http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/LaunchingAndUsingInstances.html
* https://flink.apache.org/
* http://docs.aws.amazon.com/amazondynamodb/latest/developerguide
