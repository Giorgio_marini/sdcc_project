

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import com.amazonaws.services.dynamodbv2.util.TableUtils;



public class SinkMonitoring {
    public static InfluxDB influxDB;
    private static String ipkafkaBroker;

    /**
     * Create the consumer for Topic Kafka
     *
     * @param topic
     * @return
     */
    protected static Consumer createConsumer(String topic){
        final Properties props = new Properties();
        props.put("bootstrap.servers",ipkafkaBroker+":9092");
        props.setProperty("zookeeper.connect", ipkafkaBroker+":2181");
        props.put("group.id", "SinkMonitoringConsumer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "100000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singleton(topic));
        return consumer;
    }

    /**
     * It's a first of consumer in this node and handle the topic from the monitoring and
     * consumed data.
     *
     * @param topic
     * @throws InterruptedException
     */
    static void runConsumer(String topic) throws InterruptedException{
        final Consumer<String, String> consumer = createConsumer(topic);
        final int giveUp = 10000;   int noRecordsCount = 0;

        while (true) {
            ConsumerRecords<String, String> consumerRecords =
                    consumer.poll(1000);
            for(ConsumerRecord<String,String> records: consumerRecords){
                String id, address, model;
                long timestamp;
                double consumed, tmpConsumed;
                String s = records.value();
                String[] values = s.split(",");
                id = values[1];
                address = values[2];
                model = values[3];
                tmpConsumed = Double.parseDouble(values[4]);
                consumed = round3Decimal(tmpConsumed);
                timestamp = Long.parseLong(values[0]);
                influxDB.write(Point.measurement("lampMonitoring")
                        .time(timestamp, TimeUnit.MILLISECONDS)
                        .tag("address",address)
                        .tag("id", id)
                        .tag("model" , model)
                        .addField("consumed", consumed)
                        .build());
            }
            consumerRecords.forEach(record -> System.out.printf("Consumer Record:(%s, %s)\n",
                    record.key(), record.value()));

            consumer.commitAsync();
        }

    }
    public static void main(String[] args) throws Exception {

        readArgs(args);


        //Create Table on AWS DynamoDB
        DynamoAccess.createTable(GlobalVariable.TableRanking);
        DynamoAccess.createTable(GlobalVariable.TableAlert);

        //Thread for Ranking Consumer
        Thread t = new Thread(new ConsumerKafka("Ranking", ipkafkaBroker));
        t.setName("RankingConsumer");
        t.start();

        //Thread for Alert Consumer
        Thread s = new Thread(new ConsumerKafkaAlert("Alert",ipkafkaBroker));
        s.setName("AlertConsumer");
        s.start();

        influxDB = createInfluxDb("lampSeries");
        runConsumer("Monitoring");
        influxDB.deleteDatabase("lampSeries");
    }

    private static void readArgs(String[] args) {
        if(args.length!=1){
            ipkafkaBroker="54.76.51.130";
        }else{
            ipkafkaBroker=args[0];
        }
    }
    private static double round3Decimal(double d){
        return Math.floor(d*1000)/1000;
    }

    /**
     * This method create the Database on this port, and open a ports for connect to it.
     *
     * @param name
     * @return
     */
    private static InfluxDB createInfluxDb(String name) {
        InfluxDB influxDB = InfluxDBFactory.connect("http://127.0.0.1:8086", "root", "root");
        String dbName = name;
        influxDB.createDatabase(dbName);
        influxDB.setDatabase(dbName);
        // Flush every 2000 Points, at least every 100ms
        influxDB.enableBatch(1000, 10, TimeUnit.SECONDS);
        return influxDB;
    }
}
