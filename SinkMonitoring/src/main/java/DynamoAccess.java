import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.ArrayList;
import java.util.List;

import static com.amazonaws.services.dynamodbv2.util.TableUtils.createTableIfNotExists;

/**
 * This Class handle the connection and creation of the Tables in DynamoDB
 */
public class DynamoAccess {
    public static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
    public static DynamoDB dynamoDB = new DynamoDB(client);
    public DynamoAccess(){}

    /**
     * This method allow to user to create a table in DynamoDb with a name
     * @param name
     * @throws InterruptedException
     */
    public static void createTable(String name) throws InterruptedException {

        String TableName = name;
        List<AttributeDefinition> attributeDefinitions =  new ArrayList<AttributeDefinition>();
        List<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();

        //Generate table on Dynamo Db with name;
        if(name.equals(GlobalVariable.TableRanking)) {
            attributeDefinitions.add(new AttributeDefinition().withAttributeName("Id").withAttributeType(ScalarAttributeType.S));

            keySchema.add(new KeySchemaElement().withAttributeName("Id").withKeyType(KeyType.HASH));

        }else if(name.equals(GlobalVariable.TableAlert)){
            attributeDefinitions.add(new AttributeDefinition().withAttributeName("Id").withAttributeType(ScalarAttributeType.N));
            attributeDefinitions.add(new AttributeDefinition().withAttributeName("Timestamp").withAttributeType(ScalarAttributeType.N));

            keySchema.add(new KeySchemaElement().withAttributeName("Id").withKeyType(KeyType.HASH));
            keySchema.add(new KeySchemaElement().withAttributeName("Timestamp").withKeyType(KeyType.RANGE));
        }


        CreateTableRequest request = new CreateTableRequest()
                .withTableName(TableName)
                .withKeySchema(keySchema)
                .withAttributeDefinitions(attributeDefinitions)
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(5L)
                        .withWriteCapacityUnits(6L));

        createTableIfNotExists(client,request);

        TableDescription tableDescription =
                dynamoDB.getTable(TableName).describe();
        System.out.println("create Table : " + tableDescription);
    }
}