import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * This Thread handle the topic for the Ranking. It save the records on Table in AWS DynamoDB
 */
public class ConsumerKafka implements Runnable {
    String topic;
    String ipKafkaBroker;
    Table table = DynamoAccess.dynamoDB.getTable(GlobalVariable.TableRanking);

    public ConsumerKafka(String topic, String ipKafkaBroker){
        this.topic = topic;
        this.ipKafkaBroker = ipKafkaBroker;
    }
    @Override
    public void run(){
        final Consumer<String, String> consumer = SinkMonitoring.createConsumer(topic);
        final int giveUp = 10000;   int noRecordsCount = 0;

        while (true) {
            final ConsumerRecords<String, String> consumerRecords =
                    consumer.poll(1000);

            String data ="";
            ArrayList<Item> listItem = new ArrayList<>();
            for(ConsumerRecord<String,String> records: consumerRecords){
                String[] s = records.value().split(",");
                int id = Integer.parseInt(s[1]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Long.parseLong(s[0]));
                data = ""  + calendar.get(Calendar.DATE)+ "-" +calendar.get(Calendar.MONTH)
                        +"-"+calendar.get(Calendar.YEAR);

                Item item = new Item().withPrimaryKey("Id",s[1])
                        .withLong("Timestamp", Long.parseLong(s[0]))
                        .withString("Date", data)
                        .withString("Address","" + s[2]+", "+s[3])
                        .withString("Ranking", s[4]);
                listItem.add(item);
            }
            consumerRecords.forEach(record -> System.out.printf("Consumer Record:(%s, %s)\n",
                    record.key(), record.value()));

            for (Item i :listItem) {
                table.putItem(i);
            }
            consumer.commitAsync();
        }
    }
}
