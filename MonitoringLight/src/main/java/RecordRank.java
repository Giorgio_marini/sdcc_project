import org.apache.flink.api.java.tuple.Tuple5;

public class RecordRank implements Comparable<RecordRank> {
    private long timestamp;
    private int id;
    private String address;
    private int number;
    private Long distance;

    public RecordRank(long timestamp, int id, String address, int number, Long distance) {
        this.timestamp = timestamp;
        this.id = id;
        this.address = address;
        this.number = number;
        this.distance = distance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    @Override
    public int compareTo(RecordRank recordRank) {
        if(this.distance<recordRank.distance){
            return -1;
        }else if(this.distance == recordRank.distance){
            return 0;
        } else return 1;
    }

    public Tuple5<Long, Integer, String, Integer, Long> fromRecordToTuple(){

        return new Tuple5<Long, Integer, String, Integer, Long>(this.getTimestamp(),this.id,this.address,this.number,this.distance);
    }
}
