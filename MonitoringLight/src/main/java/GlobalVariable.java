
public class GlobalVariable {

    public static final String bucket = "configfilesmart";
    public static final String key = "configAWS.json";

    public static String TOPICLamp = "LampStreaming";
    public static String TOPICMonitoring = "Monitoring";
    public static String TOPICRanking = "Ranking";
    public static String TOPICAlert = "Alert";
    public static Double hourlyEquity = 3.6;
    public static int policyWrong = 3;
    public static int avgLifeLamp = 100;

    public static String ERRORFIELD = "error into Stream";
    //TypeOfLamp
    public static String[] TypeLamp = {"alogena" , "coldled" , "hotled"};

    public static int rankingList = 10;
}
