
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class MonitoringLight {
    private static String ipKafkaBroker1;
    private static String ipKafkaBroker2;

    //public static KafkaConsumer<String, String> consumer;
    public static void main(String[] args) throws Exception {
        readArgs(args);
        settingGlobalVariable();
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1000);


        /**
         * Definition of a Flink Kafka Producer
         */
        FlinkKafkaProducer09<String> producerMonitoring = new FlinkKafkaProducer09<String>(
                ipKafkaBroker2+":9092",            // broker list
                GlobalVariable.TOPICMonitoring,                  // target topic
                new SimpleStringSchema());   // serialization schema
        producerMonitoring.setLogFailuresOnly(false);
        producerMonitoring.setFlushOnCheckpoint(true);

        FlinkKafkaProducer09<String> producerRanking = new FlinkKafkaProducer09<String>(
                ipKafkaBroker2+":9092",            // broker list
                GlobalVariable.TOPICRanking,                  // target topic
                new SimpleStringSchema());   // serialization schema
        producerMonitoring.setLogFailuresOnly(false);
        producerMonitoring.setFlushOnCheckpoint(true);

        FlinkKafkaProducer09<String> producerAlert = new FlinkKafkaProducer09<String>(
                ipKafkaBroker2+":9092",            // broker list
                GlobalVariable.TOPICAlert,                  // target topic
                new SimpleStringSchema());   // serialization schema
        producerMonitoring.setLogFailuresOnly(false);
        producerMonitoring.setFlushOnCheckpoint(true);

        /**
         * Zookeeper Server Configuration
         */
        Properties properties = initFlinkConsumer();
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        /**
         * Kafka Topic Consumer
         */
        FlinkKafkaConsumer09<String> lampStreaming = new FlinkKafkaConsumer09<>(
                GlobalVariable.TOPICLamp, new SimpleStringSchema(), properties);

        lampStreaming.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<String>(
                Time.seconds(100)) {
            @Override
            public long extractTimestamp(String s) {
                String[] event = s.split(",");

                return Long.parseLong(event[0]);
            }
        });


        /**
         * Preprocessing raw data for single lamp
         */

        DataStream<Tuple7<Long, Integer, String, Integer, String, Double, Double>> lampRawData =
                env.addSource(lampStreaming)
                    .flatMap(new serializeStream());

        DataStream<Tuple7<Long, Integer, String, Integer, String, Double, Double>> sample5minuts =
                lampRawData.keyBy(1)
                    .timeWindow(Time.minutes(5))
                    .apply(new consumedFunction());

        DataStream<Tuple7<Long, Integer, String, Integer, String, Double, Double>> sampleHourly =
                sample5minuts.keyBy(1)
                .countWindow(12)
                .apply(new functionAgg());

        DataStream<Tuple7<Long, Integer, String, Integer, String, Double, Double>> DailyConsumed =
                sampleHourly.keyBy(1)
                .countWindow(24)
                .apply(new functionAgg());

        DataStream<Tuple7<Long, Integer, String, Integer, String, Double, Double>> WeekConsumed =
                DailyConsumed.keyBy(1)
                .countWindow(7)
                .apply(new functionAgg());

        /**
         * calculate Real time monitoring on the Street
         */

        DataStream<Tuple4<Long, String, Double, Double>> sample5MinutsStreet =
                sample5minuts.keyBy(2)
                .timeWindow(Time.minutes(5))
                .apply(new aggregateLampToStreet()).keyBy(1);

        DataStream<Tuple4<Long, String, Double, Double>> HourlyConsumedStreet =
                sample5MinutsStreet.keyBy(1)
                .countWindow(12)
                .apply(new functionAggStreet());

        DataStream<Tuple4<Long, String, Double, Double>> DailyConsumedStreet =
                HourlyConsumedStreet.keyBy(1)
                .countWindow(24)
                .apply(new functionAggStreet());

        DataStream<Tuple4<Long, String, Double, Double>> WeekConsumedStreet =
                DailyConsumedStreet.keyBy(1)
                .countWindow(7)
                .apply(new functionAggStreet());


        /**
         * now, we analyse the lamp if they are broker or not
         */

        DataStream<Tuple6<Long,Integer,String,Integer,String,String>> checkStatus = env.addSource(lampStreaming)
                .flatMap(new resizeForAlert())
                .keyBy(1)
                .timeWindow(Time.minutes(5))
                .apply(new checkHealtyFunction());

        DataStream<String> Alert = checkStatus.flatMap(new identifyAlert());
        Alert.addSink(producerAlert);

        /**
         * Ranking analysis on the changing lamp
         */
        DataStream<Tuple5<Long,Integer,String,Integer,Long>> rankingChange = env.addSource(lampStreaming)
                .flatMap(new identifyMaintenance())
                .keyBy(1)
                .timeWindow(Time.minutes(5))
                .apply(new rankMaintenance());

        DataStream<Tuple5<Long,Integer,String,Integer,Long>> rank =
                rankingChange.timeWindowAll(Time.minutes(5))
                .apply(new  generateRank());

        DataStream<String> rankString = rank.flatMap(new identifyRanking());
        rankString.addSink(producerRanking);




        //Write every file on textyes

        rank.writeAsText("rankingMaintenance.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);
        sample5minuts.writeAsText("sample5minuti.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);
        sample5MinutsStreet.writeAsText("sample5minutiStreet.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);
        checkStatus.writeAsText("checkError.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        DataStream<String> MonitoredStream = sample5minuts.flatMap(new identity());
        MonitoredStream.addSink(producerMonitoring);

        env.execute();

    }

    /**
     * Setting Global variable from bucket s3
     */
    private static void settingGlobalVariable() {

        AmazonS3 s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
        try {
            System.out.println("Downloading an object");
            s3Client.getObject(
                    new GetObjectRequest(GlobalVariable.bucket, GlobalVariable.key),
                    new File("config.json")
            );

            JSONParser jsonParser = new JSONParser();
            try {
                Object obj = jsonParser.parse(new FileReader("config.json"));
                JSONObject jsonObject = (JSONObject) obj;
                JSONObject object = (JSONObject) jsonObject.get("MonitoringLight");
                GlobalVariable.TOPICLamp = object.get("TOPICLamp").toString();
                GlobalVariable.TOPICMonitoring = object.get("TOPICMonitoring").toString();
                GlobalVariable.TOPICRanking = object.get("TOPICRanking").toString();
                GlobalVariable.TOPICAlert = object.get("TOPICAlert").toString();
                GlobalVariable.hourlyEquity = Double.parseDouble(object.get("hourlyEquity").toString());
                GlobalVariable.policyWrong = Integer.parseInt(object.get("policyWrong").toString());
                GlobalVariable.avgLifeLamp = Integer.parseInt(object.get("avgLifeLamp").toString());
                GlobalVariable.ERRORFIELD = object.get("ERRORFIELD").toString();
                GlobalVariable.rankingList = Integer.parseInt(object.get("rankingList").toString());
                JSONArray ja  = (JSONArray) object.get("TypeLamp");
                String[] typeLamp = new String[ja.size()];
                for (int i = 0; i < ja.size() ; i++) {
                    typeLamp[i] = ja.get(i).toString();
                }
                GlobalVariable.TypeLamp = typeLamp;

                System.out.println(GlobalVariable.TypeLamp[1]);
            }catch (Exception e){
                e.printStackTrace();
            }

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    private static void readArgs(String[] args) {
        if(args.length!=2){
            ipKafkaBroker1="34.253.140.181";
            ipKafkaBroker2="54.76.51.130";
        }else {
            ipKafkaBroker1=args[0];
            ipKafkaBroker2=args[1];
        }
    }

    /**
     * Init of a Flink Consumer
     * @return
     */
    private static Properties initFlinkConsumer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", ipKafkaBroker1+":9092");
        properties.setProperty("zookeeper.connect", ipKafkaBroker1+":2181");
        properties.setProperty("group.id", "monitoring");
        return properties;
    }

    /**
     * serialize stream with identity funcion
     */
    private static class identity implements FlatMapFunction<Tuple7<Long, Integer, String,Integer, String, Double, Double>, String>{
        @Override
        public void flatMap(Tuple7<Long, Integer, String,Integer, String, Double, Double> t, Collector<String> collector) throws Exception {
            String result = ""+t.f0+","+t.f1+","+t.f2+","+t.f4+","+t.f6;
            collector.collect(result);

        }
    }

    /**
     * serialize input
     */
    private static class serializeStream implements FlatMapFunction< String,Tuple7<Long, Integer, String, Integer, String, Double, Double>> {
        @Override
        public void flatMap(String s, Collector<Tuple7<Long, Integer, String, Integer, String, Double, Double>> collector) throws Exception {
            String[] toParse = s.split(",");
            long timestamp = Long.parseLong(toParse[0]);
            int key = Integer.parseInt(toParse[1]);
            String address = toParse[2];
            int number = Integer.parseInt(toParse[3]);
            String model = toParse[4];
            double consumed = Double.parseDouble(toParse[6]);
            double intensityLight = Double.parseDouble(toParse[7]);
            collector.collect(new Tuple7<>(timestamp,key,address,number,model,consumed,intensityLight));
        }
    }

    /**
     * mean of consumed current in t-time and converter the measure unit W/s to KW/h
     */
    private static class consumedFunction implements WindowFunction<Tuple7<Long, Integer, String, Integer, String, Double, Double>,
                                                                Tuple7<Long, Integer, String, Integer, String, Double, Double>, Tuple, TimeWindow> {

        private Tuple7<Long, Integer, String, Integer, String, Double, Double> t = new Tuple7<>(0L,0,"",0,"",0.0,0.0);
        private int count = 0;
        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple7<Long, Integer, String, Integer, String, Double, Double>> iterable, Collector<Tuple7<Long, Integer, String, Integer, String, Double, Double>> collector) throws Exception {
            double consumed = 0.0;

            for (Tuple7<Long, Integer, String, Integer, String, Double, Double> tmp: iterable) {
                consumed = consumed + tmp.f5;
                t = tmp;
                count ++;
            }
            double avgWattXsecondi = consumed/count;
            t.f5 = avgWattXsecondi;
            double consumoKWatth = avgWattXsecondi*GlobalVariable.hourlyEquity/12;
            t.f6 = consumoKWatth;
            t.f0 = timeWindow.getEnd();
            count = 0;
            collector.collect(t);
        }
    }

    /**
     * This function aggregate the current consume by the hours
     */
    private static class functionAgg implements WindowFunction <Tuple7<Long,Integer,String,Integer,String,Double,Double>,
                Tuple7<Long,Integer,String,Integer,String,Double,Double>, Tuple, GlobalWindow>{

        private Tuple7<Long,Integer,String,Integer,String,Double,Double> aux;
        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow, Iterable<Tuple7<Long, Integer, String, Integer, String, Double, Double>> iterable, Collector<Tuple7<Long, Integer, String, Integer, String, Double, Double>> collector) throws Exception {
            double avgWattXminuts, sum = 0.0;
            int count = 0;

            double totalConsumedhourlyEffective = 0.0;
            for (Tuple7<Long,Integer, String, Integer, String, Double, Double> t: iterable) {
                totalConsumedhourlyEffective += t.f6;
                sum += t.f5;
                count++;

                aux = t;
            }
            avgWattXminuts = sum/count;
            // measure unit = kwh
            double avgTotalConsumed = avgWattXminuts*3.6;
            aux.f5 = avgTotalConsumed;
            aux.f6 = totalConsumedhourlyEffective;
            collector.collect(aux);
        }
    }

    /**
     * This function aggregate the value of current consumed in the single street
     */
    private static class functionAggStreet implements WindowFunction<Tuple4<Long,String,Double,Double>, Tuple4<Long,String,Double,Double>,
            Tuple,GlobalWindow> {

        private Tuple4<Long,String,Double,Double> aux;

        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow, Iterable<Tuple4<Long, String, Double, Double>> iterable, Collector<Tuple4<Long, String, Double, Double>> collector) throws Exception {
            double avgWattXminuts, sum = 0.0;
            int count = 0;

            double totalConsumedhourlyEffective = 0.0;
            for (Tuple4<Long, String, Double, Double> t: iterable) {
                totalConsumedhourlyEffective += t.f3;
                sum += t.f2;
                count++;

                aux = t;
            }
            avgWattXminuts = sum/count;

            // measure unit = kwh
            // double avgTotalConsumed = avgWattXminuts*3.6;
            // measure f2 avg consumed
            aux.f2 = avgWattXminuts;
            // measure f3 consumed effective
            aux.f3 = totalConsumedhourlyEffective;
            collector.collect(aux);
        }
    }

    /**
     * aggregate the Lamp to a single street
     */
    private static class aggregateLampToStreet implements WindowFunction<Tuple7<Long,Integer,String,Integer,String,Double,Double>
            ,Tuple4<Long,String,Double,Double>,Tuple,TimeWindow> {
        Tuple4<Long,String,Double,Double> aux;
        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple7<Long, Integer, String, Integer, String, Double, Double>> iterable, Collector<Tuple4<Long, String, Double, Double>> collector) throws Exception {
            double consumedWat = 0.0;
            double sumWat = 0.0;
            int count = 0;
            String address ="";

            for (Tuple7<Long, Integer, String, Integer, String, Double, Double> t: iterable){
                consumedWat += t.f6;
                sumWat += t.f5;
                count++;
                address = t.f2;
            }
            double avgWatt = sumWat/count;
            collector.collect(new Tuple4<Long,String,Double,Double>(timeWindow.getEnd(), address,avgWatt,consumedWat));
        }
    }

    /**
     * Resize Stream for calculate alert
     */
    private static class resizeForAlert implements FlatMapFunction<String,Tuple6<Long,Integer,String,Integer,String,String>>{

        @Override
        public void flatMap(String s, Collector<Tuple6<Long, Integer, String, Integer, String,String>> collector) throws Exception {
            String[] toParse = s.split(",");
            long timestamp = Long.parseLong(toParse[0]);
            int id = Integer.parseInt(toParse[1]);
            String address = toParse[2];
            int number = Integer.parseInt(toParse[3]);
            String model = toParse[4];
            String status = toParse[5];

            Boolean state = checkConsistencyTuple(toParse);

            if(state == true){
                Tuple6<Long, Integer, String, Integer, String,String> t = new Tuple6<>(timestamp,id,address,number,status,model);
                collector.collect(t);
            }
            else collector.collect(new Tuple6<>(timestamp,id,address,number,GlobalVariable.ERRORFIELD,model));
        }

        /**
         * Control the consistency of the tuples in their structure and field values
         * @param s
         * @return
         * true if the tuple is correct else false if it's wrong
         */
        private boolean checkConsistencyTuple(String[] s) {
            if(s.length !=10){
                return false;
            }
            if(checkDate(s[9])==false){
                return false;
            }
            if(Double.parseDouble(s[6]) < 0){
                return false;
            }
            if(Double.parseDouble(s[7])<0){
                return false;
            }
            if(Double.parseDouble(s[8])<0){
                return false;
            }
            if(checkModel(s[4])==false){
                return false;
            }
            return true;
        }

        private boolean checkModel(String s) {

            for (int i = 0; i <GlobalVariable.TypeLamp.length ; i++) {
                if(s.equals(GlobalVariable.TypeLamp[i])){
                    return true;
                }
            }
            return false;

        }

        private boolean checkDate(String s) {
            if(s.length()!=10){
                return false;
            }
            return true;
        }
    }

    /**
     * This function control if the record is good
     */
    private static class checkHealtyFunction implements WindowFunction<Tuple6<Long,Integer,String,Integer,String,String>,Tuple6<Long,Integer,String,Integer,String,String>,
            Tuple,TimeWindow> {

        private Tuple6<Long,Integer,String,Integer,String,String> aux;

        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple6<Long, Integer, String, Integer, String,String>> iterable, Collector<Tuple6<Long, Integer, String, Integer, String,String>> collector) throws Exception {
            int count = 0, countBreak = 0;
            for (Tuple6<Long, Integer, String, Integer, String,String> t: iterable) {
                count++;
                if (!t.f4.equals(GlobalVariable.ERRORFIELD)) {
                    if (getPolicy() == true) {
                        if (t.f4.equals("false")) {
                            countBreak++;
                        }
                    }
                    aux = t;
                }else countBreak++;
            }
            if(countBreak > GlobalVariable.policyWrong) {
                String state = "error on Lamp";
                collector.collect(new Tuple6<>(timeWindow.getEnd(),aux.f1,aux.f2,aux.f3,state,aux.f5));
            }
        }

        private boolean getPolicy() {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            int h = date.getHours();
            if (h < 18 && h >= 6) {
                return false;
            }else return true;
        }
    }

    /**
     * generate the ranking of maintenance of lamps
     */
    private static class generateRank implements AllWindowFunction<Tuple5<Long, Integer, String, Integer, Long>,Tuple5<Long, Integer, String, Integer, Long> , TimeWindow> {

        @Override
        public void apply(TimeWindow timeWindow, Iterable<Tuple5<Long, Integer, String, Integer, Long>> iterable, Collector<Tuple5<Long, Integer, String, Integer, Long>> collector) throws Exception {
            Set<RecordRank> ranking = new TreeSet<>();
            for (Tuple5<Long, Integer, String, Integer, Long> t: iterable) {
                RecordRank r = new RecordRank(t.f0,t.f1,t.f2,t.f3,t.f4);
                ranking.add(r);
            }
            int count = 0;
            for (RecordRank r: ranking) {
                if(count==GlobalVariable.rankingList){
                    break;
                }
                collector.collect(r.fromRecordToTuple());
                count ++;
            }

        }
    }

    private static class identifyMaintenance implements FlatMapFunction<String, Tuple5<Long,Integer,String,Integer,String>>{
        @Override
        public void flatMap(String s, Collector<Tuple5<Long, Integer, String, Integer, String>> collector) throws Exception {
            String[] toParse = s.split(",");
            long timestamp = Long.parseLong(toParse[0]);
            int id = Integer.parseInt(toParse[1]);
            String address = toParse[2];
            int number = Integer.parseInt(toParse[3]);
            String date = toParse[9];
            collector.collect(new Tuple5<>(timestamp,id,address,number,date));
        }
    }

    private static class rankMaintenance implements WindowFunction<Tuple5<Long, Integer, String, Integer, String>,
            Tuple5<Long, Integer, String, Integer, Long>,Tuple, TimeWindow> {
        private Tuple5<Long, Integer, String, Integer,String> aux;
        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple5<Long, Integer, String, Integer, String>> iterable,
                          Collector<Tuple5<Long, Integer, String, Integer, Long>> collector) throws Exception {
            long distance=0;
            for (Tuple5<Long, Integer, String, Integer, String> t: iterable) {
                aux = t;

                String target = t.f4;
               /* DateFormat df = new SimpleDateFormat("E MM dd kk:mm:ss z yyyy");
                Date result =  df.parse(target);*/
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse(target);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                long result = cal.getTime().getTime();
                Date now = new Date();
                now.getTime();

                /*
                calculate distance for ranking of maintenance
                 */
                long dayChange  = result/(1000*(3600*24));
                long nowDay     =  now.getTime()/(1000*(3600*24));
                distance = GlobalVariable.avgLifeLamp-(nowDay-dayChange);
                break;
            }
            collector.collect(new Tuple5<>(timeWindow.getEnd(), aux.f1,aux.f2,aux.f3, distance));
        }
    }

    private static class identifyAlert implements FlatMapFunction<Tuple6<Long,Integer,String,Integer,String,String>,String> {

        @Override
        public void flatMap(Tuple6<Long, Integer, String, Integer, String, String> in, Collector<String> collector) throws Exception {
            collector.collect(in.f0+","+in.f1+","+in.f2+","+in.f3+","+in.f4+","+in.f5);
        }
    }

    private static class identifyRanking implements FlatMapFunction<Tuple5<Long,Integer,String,Integer,Long>,String>{
        @Override
        public void flatMap(Tuple5<Long, Integer, String, Integer, Long> in, Collector<String> collector) throws Exception {
            collector.collect(in.f0+","+in.f1+","+in.f2+","+in.f3+","+in.f4);
        }
    }










    /*public static void initKafkaConsumer() {

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.setProperty("zookeeper.connect", "localhost:2181");
        props.put("group.id", "lampConsumer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "100000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(GlobalVariable.TOPICLamp));
    }*/
}


