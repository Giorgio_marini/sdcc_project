import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Generate fields of the records. there is some policies to producer the right output
 */
public class GenerateFieldStream{
    public long getTimestamp(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        return date.getTime();
    }

    /**
     * Init the status of the lamp
     * @return
     */
    public boolean getOnOff() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        if(date.getHours()>16 || date.getHours()<7){
            if(Math.random()>0.98){
                return false;
            }else return true;
        }else return false;
    }

    /**
     * make the intensity of the Sun Lamp
     * @return
     */
    public double getIntensitySun() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        double intensitySun = 0;
        int h = date.getHours();
        if (h < 17 && h > 6) {
            intensitySun = 10.0 - Math.random();
        } else if (h >= 17 && h < 18) {
            intensitySun = 7.5 - Math.random();
        }else if(h == 18){
            intensitySun = 6.0 - Math.random();
        } else if (h >= 19 && h < 20) {
            intensitySun = 5.0 - Math.random();
        } else if (h >= 20 && h < 21) {
            intensitySun = 2.0 - Math.random();
        } else if (h >= 21 || h <=6) {
            intensitySun = 0.0;
        }
        return intensitySun;
    }

    public double getIntensityLight(double intensitySun,boolean onOff) {
        double intensityLight;
        if(onOff == true) {
            if(intensitySun>=7){
                intensityLight=0;
            }else {
                intensityLight = 10.0;
            }
            return intensityLight;
        }else return 0;
    }

    /**
     * generate the value of the consumed current in function of the lamp type
     * @param s
     * @param intensityLight
     * @return
     */
    public double getConsumed(ArrayList<String> s, double intensityLight) {
        double consumed=0;
        if(intensityLight == 0.0){
            return 0.0;
        }else
        if(s.get(4).equals("coldled")){
            consumed = GlobalVariable.WattSecondCOLDLED+ Math.random()*intensityLight;
        }else if(s.get(4).equals("hotled")){
         consumed = GlobalVariable.WattSecondHOTLED + Math.random()*intensityLight;
        }else if(s.get(4).equals("alogena")){
            consumed = GlobalVariable.WattSecondAlogena + Math.random()*intensityLight;
        }
        return consumed;
    }

}
