
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import org.apache.kafka.clients.producer.Producer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.util.*;


public class LampStreaming {

    private static Producer<String,String> producer;
    public static ArrayList<MessageStream> messageStreams;
    public static String ipKafkaBroker1;

    public LampStreaming() {
        this.messageStreams = new ArrayList<>();
    }

    public ArrayList<MessageStream> getMessageStreams() {
        return messageStreams;
    }

    /**
     * Start-up phase, read from a json file and init the structure data.
     * @param pathFile
     */
    public void StartUpJson(String pathFile){

        long lampIndex;
        String address;
        int number;
        String model;
        double consumed;
        boolean onOff;
        double intensityLight;
        String lastReplace;
        long timestamp;
        double intensitySunLight;

        JSONParser jsonParser = new JSONParser();
        try {
            Object obj = jsonParser.parse(new FileReader(pathFile));
            JSONArray jsonArray = (JSONArray)obj;
            for (int i = 0; i <jsonArray.size() ; i++) {
                JSONObject jsonObject = (JSONObject)jsonArray.get(i);
                timestamp = new Date().getTime();
                lampIndex = ((long) jsonObject.get("index"));
                address = jsonObject.get("address").toString();
                number = Integer.parseInt(jsonObject.get("number").toString());
                model = jsonObject.get("model").toString();
                consumed = Double.parseDouble(jsonObject.get("consumed").toString());
                onOff = (Boolean)jsonObject.get("isActive");
                intensityLight = 0.0;
                intensitySunLight = 0.0;
                lastReplace = (String) jsonObject.get("registered");
                MessageStream msg = new MessageStream(lampIndex,address,number,model,consumed,onOff,intensityLight,
                        lastReplace,timestamp,intensitySunLight);

                messageStreams.add(msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static String joined(ArrayList<String> msg) {
        String s = String.join(",", msg);
        return s;
    }

    /**
     * It handle the generation of the events, make a random record by some policy
     * @param strings
     * @return
     */
    public static String generateEvent(ArrayList<String> strings){
        GenerateFieldStream generateFieldStream = new GenerateFieldStream();
        long timestamp = generateFieldStream.getTimestamp();
        boolean onOff = generateFieldStream.getOnOff();
        double intensitySun = generateFieldStream.getIntensitySun();
        double intensityLight; //generateFieldStream.getIntensityLight(intensitySun,onOff);
        intensityLight = Double.parseDouble(strings.get(7));
        double consumed = generateFieldStream.getConsumed(strings,intensityLight);

        strings.set(0,Long.toString(timestamp));
        strings.set(5,Boolean.toString(onOff));
        strings.set(6,Double.toString(consumed));
        //strings.set(7,Double.toString(intensityLight));
        strings.set(8,Double.toString(intensitySun));

        String record = joined(strings);
        return record;
    }

    public static void main(String[] args) throws Exception {
        readArgs(args);
        settingGlobalVariable();
        LampStreaming lampStreaming = new LampStreaming();
        lampStreaming.StartUpJson(GlobalVariable.pathFile);
                for (int i = 0; i <lampStreaming.getMessageStreams().size(); i++) {
            System.out.println(lampStreaming.joined(lampStreaming.getMessageStreams().get(i).getMsg()));
        }
        System.out.println("**************");

        Controller c = new Controller();
        Thread t = new Thread(c);
        t.start();
        System.out.println("thread id: " + t.getId());

        int NumberOfThreadThread = messageStreams.size()/GlobalVariable.lampForThread;

        for (int k = 0; k < NumberOfThreadThread ; k++) {

            ProducerLampStream producerLampStream = new
                    ProducerLampStream(k*GlobalVariable.lampForThread, (k+1)*GlobalVariable.lampForThread);
                    Thread producer = new Thread(producerLampStream);
                    producer.start();
                    System.out.println("thread producer " + k + " , " + producer.getId());
        }
    }

    /**
     * Setting global variable from S3 file
     */
    private static void settingGlobalVariable() {

        AmazonS3 s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
        try {
            System.out.println("Downloading an object");
            s3Client.getObject(
                    new GetObjectRequest(GlobalVariable.bucket, GlobalVariable.key),
                    new File("config.json")
            );

            JSONParser jsonParser = new JSONParser();
            try {
                Object obj = jsonParser.parse(new FileReader("config.json"));
                JSONObject jsonObject = (JSONObject) obj;
                JSONObject object = (JSONObject) jsonObject.get("LampStreaming");
                GlobalVariable.pathFile = object.get("pathFile").toString();
                GlobalVariable.TOPICLamp = object.get("TOPICLamp").toString();
                GlobalVariable.TOPIControl = object.get("TOPIControl").toString();
                GlobalVariable.TOPICLight = object.get("TOPICLight").toString();
                GlobalVariable.WattSecondAlogena = Integer.parseInt(object.get("WattSecondAlogena").toString());
                GlobalVariable.WattSecondCOLDLED = Integer.parseInt(object.get("WattSecondCOLDLED").toString());
                GlobalVariable.WattSecondHOTLED = Integer.parseInt(object.get("WattSecondHOTLED").toString());
                GlobalVariable.lampForThread = Integer.parseInt((object.get("lampForThread").toString()));
            }catch (Exception e){
                e.printStackTrace();
            }

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    private static void readArgs(String[] args) {
        if(args.length!=1){
            ipKafkaBroker1="34.253.140.181";
        }else {
            ipKafkaBroker1 = args[0];
        }
    }
}
