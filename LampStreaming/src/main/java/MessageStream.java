import java.util.ArrayList;
import java.util.Date;

/**
 * A Structure to handle the records with their dynamic and static fields
 */
public class MessageStream {
    private long lampIndex;
    private String address;
    private int number;
    private String model;
    private double consumed;
    private boolean onOff;
    private double intensityLight;
    private String lastChange;
    private long timestamp;
    private double intensitySunLight;

    private ArrayList<String> msg;


    public MessageStream(long lampIndex, String address, int number, String model, double consumed,
                         boolean onOff, double intensityLight, String lastChange, long timestamp, double intensitySunLight) {
        this.lampIndex = lampIndex;
        this.address = address;
        this.number = number;
        this.model = model;
        this.consumed = consumed;
        this.onOff = onOff;
        this.intensityLight = intensityLight;
        this.lastChange = lastChange;
        this.timestamp = timestamp;
        this.intensitySunLight = intensitySunLight;
        msg = new ArrayList<>();
        msg.add(Long.toString(timestamp));
        msg.add(Long.toString(lampIndex));
        msg.add(address);
        msg.add(Integer.toString(number));
        msg.add(model);
        msg.add(Boolean.toString(onOff));
        msg.add(Double.toString(consumed));
        msg.add(Double.toString(intensityLight));
        msg.add(Double.toString(intensitySunLight));
        msg.add(lastChange);
    }

    public long getLampIndex() {
        return lampIndex;
    }

    public void setLampIndex(long lampIndex) {
        this.lampIndex = lampIndex;
        this.msg.set(1,Long.toString(lampIndex));
    }

    public String getAddress() {
        return address;

    }

    public void setAddress(String address) {
        this.address = address;
        this.msg.set(2,address);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
        this.msg.set(3, Integer.toString(number));
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
        this.msg.set(4,model);
    }

    public boolean isOnOff() {
        return onOff;
    }

    public void setOnOff(boolean onOff) {
        this.onOff = onOff;
        this.msg.set(5,Boolean.toString(onOff));
    }

    public double getConsumed() {
        return consumed;
    }

    public void setConsumed(double consumed) {
        this.consumed = consumed;
        this.msg.set(6,Double.toString(consumed));
    }

    public double getIntensityLight() {
        return intensityLight;
    }

    public void setIntensityLight(double intensityLight) {
        this.intensityLight = intensityLight;
        this.msg.set(7,Double.toString(intensityLight));
    }

    public double getIntensitySunLight() {
        return intensitySunLight;
    }

    public void setIntensitySunLight(double intensitySunLight) {
        this.intensitySunLight = intensitySunLight;
        this.msg.set(8,Double.toString(intensitySunLight));
    }
    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
        this.msg.set(9,""+lastChange);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ArrayList<String> getMsg() {
        return msg;
    }

    public void setMsg(ArrayList<String> msg) {
        this.msg = msg;
    }
}
