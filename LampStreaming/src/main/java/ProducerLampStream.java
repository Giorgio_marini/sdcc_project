import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.ArrayList;
import java.util.Properties;

public class ProducerLampStream implements Runnable {
    int i,y;
    ProducerLampStream(int extr1, int extr2){
        this.i = extr1;
        this.y = extr2;
    }
    @Override
    public synchronized void run(){
        Producer<String,String > producer;
        Properties props = initProperties();
        producer = new KafkaProducer<String, String>(props);

        while(true) {
            for (int l = i; l < y; l++) {
                String record = LampStreaming.generateEvent(LampStreaming.messageStreams.get(l).getMsg());
                sendLampRecord(record,producer);
                sendSunnyLight(record,producer);
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * send Lamp records to Kafka Broker
     * @param record
     * @param producer
     */
    public static void sendLampRecord(String record, Producer<String,String> producer){
        String[] s = record.split(",");
        String key = s[1];
        //System.out.println("key: " + key + "status "+ record);
        System.out.println("record -> "+ record);
        producer.send(new ProducerRecord<String, String>(GlobalVariable.TOPICLamp,GlobalVariable.TOPICLamp,record));

    }

    /**
     * Send Sun records to Kafka Broker
     * @param record
     * @param producer
     */
    public static void sendSunnyLight(String record, Producer<String,String> producer){
        String[] tmp = record.split(",");
        String key = tmp[1];
        ArrayList<String> recordSun = new ArrayList<>();
        recordSun.add(tmp[0]);
        recordSun.add(tmp[1]);
        recordSun.add(tmp[8]);
        String sunLightRecord = joined(recordSun);
        System.out.println("record -> "+ sunLightRecord);
        producer.send(new ProducerRecord<String, String>(GlobalVariable.TOPICLight,GlobalVariable.TOPICLight,sunLightRecord));
    }
    private Properties initProperties() {
        Properties props = new Properties();
        //Assign localhost id
        props.put("bootstrap.servers", LampStreaming.ipKafkaBroker1+":9092");
        //Set acknowledgements for producer requests.
        props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    public static String joined(ArrayList<String> msg) {
        String s = String.join(",", msg);
        return s;
    }

}
