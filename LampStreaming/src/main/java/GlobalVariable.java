public class GlobalVariable {
    public static final String bucket = "configfilesmart";
    public static final String key = "configAWS.json";

    public static String pathFile = "./src/lampStartup1.json";
    public static String TOPICLamp = "LampStreaming";
    public static String TOPIControl  = "ControlLamp";
    public static String TOPICLight = "SunLight";

    //Consumed Watt/second
    public static  int WattSecondAlogena = 150;
    public static  int WattSecondHOTLED = 30;
    public static  int WattSecondCOLDLED = 25;


    public static int lampForThread = 100;
}
