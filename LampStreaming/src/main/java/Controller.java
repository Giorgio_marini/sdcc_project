import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Collections;
import java.util.Properties;

public class Controller implements Runnable {

    Controller(){
    }

    /**
     * The main function manage the arrived records from controller topic, and re-write the value
     * in the structure.
     */
    @Override
    public void run() {
        final Consumer<String, String> consumer = createConsumer();
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);
            for (ConsumerRecord<String, String> record : records){
                String[] tmp = record.value().split(",");
                int key = Integer.parseInt(tmp[1]);
                double correctLight = Double.parseDouble(tmp[2]);
                MessageStream m = LampStreaming.messageStreams.get(key);
                m.setIntensityLight(correctLight);
                System.out.println("refactor Intensity of Light of the lamp:" + key);
                LampStreaming.messageStreams.set(key,m);
                System.out.println("Record" + LampStreaming.messageStreams.get(key).getMsg());

            }
        }

    }
    private Consumer<String, String> createConsumer(){

        final Properties props = new Properties();
        props.put("bootstrap.servers", LampStreaming.ipKafkaBroker1+":9092");
        props.setProperty("zookeeper.connect", LampStreaming.ipKafkaBroker1+":2181");
        props.put("group.id", "lampController");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "100000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singleton(GlobalVariable.TOPIControl));
        return consumer;
    }

}
